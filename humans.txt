/* bextlan.com | lugar de... bits, vectores y pixeles. -http://bextlan.com */ 

/* TEAM */
	Creator: Grover Rendich
	Contact: cursos [at] bextlan.com
	Twitter: @grendich
	From: Huancayo, Peru
	
/* IN MEMORIAL */
	Marcelito, Ricardo, Brandon, y todos los seres queridos que ya no estan con nosotros.
	
/* THANKS */
	Bextlan.com, Marcel Rendich, Maggaly, Gabriela
	Ammy, Jorge
	ICONOS http://www.iconos.edu.mx
	
/* SITE */
	Las update: 2012/11/08
	Language: Espa�ol
	Standars: HTML5, CSS3, JavaScript
	Components: jQuery
	Software: SublimeText2, Notepad++, Adobe Suite CS5 (Flash, Photoshop, Fireworks)